
[![License: CC BY-SA-NC 4.0](https://img.shields.io/badge/License-CC%20BY--SA--NC%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa-nc/4.0/)

## Autumn

A map made based on a design by Blodir as part of a community effort on map designs. For more information:
 - https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout

![](images/overview-1.PNG)

## Statistics on the map

The map is designed for competitive play. There are numerous choices to expand to early on and three relative large reclaim fields on the diagonal between the players. Both players will need to make a choice on which field to reclaim or contest.

There is reclaim on this map. This includes: 
 - Two large tree fields in the north and south of the map.
 - Three large rock fields on the diagonal between the players.

![](/images/overview-2.PNG)

## Technical aim of the map

The map took over 20 hours to make. The aim was on providing a competitive map while being aesthetically pleasing. According to some I've succeeded in doing so. But personally I'm not entirely satisfied with it. The tree fields and rock fields could be improved a lot, with additional detail that fits the field.

The pipeline for making maps was improved even further. Via the use of World Machine scripts some parts could be done in sequence without any interference. This allows me to leave the computer to do the groceries while the computer performs every group of nodes in sequence. A lot of nodes are grouped with input and output nodes to prevent using too much RAM.

![](/images/overview-3.PNG)

## Installation guide

The map is available in the [FAF](https://www.faforever.com/) map vault and in the [LOUD](https://www.moddb.com/mods/loud-ai-supreme-commander-forged-alliance) map library. They have a different marker layout.

For the FAF-version store the map in the following folder:
 - %USER%/My Documents/My Games/Gas Powered Games/Supreme Commander Forged Alliance/maps

For the LOUD-version store the map in the following folder:
 - %STEAM%/userapps/common/Supreme Commander Forged Alliance/LOUD/usermaps

If the map folder does not exist in either situation then you can freely make one with the corresponding name.

## Map preview

![](/images/marked-preview.png)

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
