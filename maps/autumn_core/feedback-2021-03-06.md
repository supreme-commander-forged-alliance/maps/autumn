[ ] locations with no trees -> more dirt-based
[ ] top -right / bottom -left corner should be more open palms-isch
 - idea: edge building it / transporting, valuable but vulnerable. Hard to attack / defend (more build space), doesn't have to be square. Should be (grouped) trees that are worth getting
[ ] More single trees near the spawn, encouraging expanding
[ ] more ramp visibility, single color? -> less white
[x] forest area's pop out too much
[x] flat area's are cool
[ ] forests are placed to allow you to prevent crushing groups
[ ] reclaim area's (spread out enough to take out the engineers), shouldn't be too fast, reasonable amount -> small rocks, no biggies
-> separate rocks / trees
[x] mini-mountains, make it flow into it more
[x] roads, make them connect each other -> more game info about where things are flat 
[x] treadmarks of tanks, etc, in the terrain