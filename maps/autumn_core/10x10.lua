Decals = {
    --
    ['9007'] = {
        type = 'Albedo',
        name0 = '/maps/autumn.v0001/env/decals/concept.dds',
        name1 = '',
        scale = { 512, 512, 512 },
        position = { 0, 0, 0 },
        euler = { -0, 0, 0 },
        far_cutoff = 10000,
        near_cutoff = 0,
        remove_tick = 0,
    },
}
