version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Autumn",
    description = "Map made based on the design of Blodir from: https://forum.faforever.com/topic/1214/a-community-effort-on-a-map-layout/8",
    preview = '',
    map_version = 1,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {0, 0},
    map = '/maps/autumn_core/autumn.scmap',
    save = '/maps/autumn_core/autumn_save.lua',
    script = '/maps/autumn_core/autumn_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
