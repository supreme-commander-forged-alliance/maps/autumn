version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Autumn",
    description = "An artificial valley crafted with the sole purpose to be a private battle ground. \r\n\r\nMap is made by (Jip) Willem Wijnia. \r\nMap is based on a design made by Blodir.\r\n\r\nTextures are from www.textures.com or cc0textures.com. \r\n\r\nLicensed with CC-BY-SA-NC 4.0.\r\n\r\nFor more information: https://gitlab.com/supreme-commander-forged-alliance/maps/autumn",
    preview = '',
    map_version = 3,
    type = 'skirmish',
    starts = true,
    size = {512, 512},
    reclaim = {18711.91, 116019.3},
    map = '/maps/autumn.v0003/autumn.scmap',
    save = '/maps/autumn.v0003/autumn_save.lua',
    script = '/maps/autumn.v0003/autumn_script.lua',
    norushradius = 40,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_17 NEUTRAL_CIVILIAN' ),
            },
        },
    },
}
